﻿namespace Service.Job
{
    using Domain.Candidate;
    using Domain.Job;
    using Infrastructure.JobAdder;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;


    public class JobCandidateRetriever : IJobCandidateRetriever
    {

        private readonly IJobAdderService _jobAdderService;

        public JobCandidateRetriever(IJobAdderService jobAdderService)
        {
            _jobAdderService = jobAdderService;
        }

        public async Task<IList<JobCandidate>> GetJobCandidatesAsync(int jobId)
        {
            Job job = await _jobAdderService.GetJobAsync(jobId);

            IList<Candidate> canditates = await _jobAdderService.GetCandidatesAsync();

            IList<JobCandidate> jobCandidates =
                canditates
                .Select(x => new JobCandidate
                {
                    CandidateName = x.Name,
                    JobMatchSkills = x.SkillTagItems.Intersect(job.SkillItems).ToList(),
                    TotalJobMatchSkills = x.SkillTagItems.Intersect(job.SkillItems).Count(),
                    Skills = x.SkillTagItems
                })
                .Where(x => x.TotalJobMatchSkills > 0)
                .OrderByDescending(x => x.TotalJobMatchSkills)
                .ToList();

            return jobCandidates;
        }
    }
}
