﻿namespace Domain.Job
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IJobCandidateRetriever
    {
        Task<IList<JobCandidate>> GetJobCandidatesAsync(int jobId);
    }
}
