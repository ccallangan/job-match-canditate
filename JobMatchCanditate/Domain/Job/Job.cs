﻿namespace Domain.Job
{
    using System.Collections.Generic;
    using System.Linq;

    public class Job
    {

        public int JobId { get; set; }

        public string Name { get; set; }

        public string Company { get; set; }

        public string Skills { get; set; }

        public IList<string> SkillItems
        {
            get
            {
                IList<string> skills = new List<string>();

                if (!string.IsNullOrEmpty(Skills))
                {
                    return Skills
                            .Split(',')
                            .Select(x => x)
                            .Distinct()
                            .ToList();
                }

                return skills;
            }
        }

    }
}
