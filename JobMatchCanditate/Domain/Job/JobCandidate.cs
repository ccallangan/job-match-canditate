﻿
namespace Domain.Job
{
    using System.Collections.Generic;

    public class JobCandidate
    {

        public string CandidateName { get; set; }

        public IList<string> Skills { get; set; }

        public IList<string> JobMatchSkills { get; set; }

        public int TotalJobMatchSkills { get; set; }

    }
}

