﻿namespace Domain.Candidate
{
    using System.Collections.Generic;
    using System.Linq;

    public class Candidate
    {

        public int CandidateId { get; set; }

        public string Name { get; set; }

        public string SkillTags { get; set; }

        public IList<string> SkillTagItems
        {
            get
            {
                IList<string> skillTags = new List<string>();

                if (!string.IsNullOrEmpty(SkillTags))
                {
                    return SkillTags
                            .Split(',')
                            .Select(x => x)
                            .Distinct()
                            .ToList();
                }

                return skillTags;
            }
        }

    }
}
