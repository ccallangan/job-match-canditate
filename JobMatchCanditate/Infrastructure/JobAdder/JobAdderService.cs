﻿namespace Infrastructure.JobAdder
{
    using Domain.Candidate;
    using Domain.Job;
    using Infrastructure.Network;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;


    public interface IJobAdderService
    {
        Task<IList<Job>> GetJobsAsync();

        Task<Job> GetJobAsync(int jobId);

        Task<IList<Candidate>> GetCandidatesAsync();
    }


    public class JobAdderService : IJobAdderService
    {
        private const string AddressTemplate = "http://private-76432-jobadder1.apiary-mock.com/";
        private readonly IWebClientWrapper _webClientWrapper;

        public JobAdderService(IWebClientWrapper webClientWrapper)
        {
            _webClientWrapper = webClientWrapper;
        }

        public async Task<IList<Job>> GetJobsAsync()
        {
            IList<Job> result = new List<Job>();

            string itemsAsString = await _webClientWrapper.GetData($"{AddressTemplate}/jobs");

            if (!string.IsNullOrEmpty(itemsAsString))
            {
                result = JsonConvert.DeserializeObject<List<Job>>(itemsAsString);
            }

            return result;
        }

        public async Task<Job> GetJobAsync(int jobId)
        {
            IList<Job> jobs = await GetJobsAsync();

            return jobs.First(x => x.JobId == jobId);
        }

        public async Task<IList<Candidate>> GetCandidatesAsync()
        {
            IList<Candidate> result = new List<Candidate>();

            string itemsAsString = await _webClientWrapper.GetData($"{AddressTemplate}/candidates");

            if (!string.IsNullOrEmpty(itemsAsString))
            {
                result = JsonConvert.DeserializeObject<List<Candidate>>(itemsAsString);
            }
            return result;
        }

    }
}
