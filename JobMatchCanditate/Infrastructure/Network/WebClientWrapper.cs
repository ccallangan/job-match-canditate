﻿namespace Infrastructure.Network
{

    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;

    public class WebClientWrapper : IWebClientWrapper
    {
        public async Task<string> GetData(string url)
        {
            HttpClient httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await httpClient.GetAsync(new Uri(url)).ConfigureAwait(false);

            string itemsAsString = string.Empty;

            if (response.IsSuccessStatusCode)
            {
                itemsAsString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            }

            return itemsAsString;
        }
    }
}
