﻿namespace Infrastructure.Network
{
    using System.Threading.Tasks;


    public interface IWebClientWrapper
    {
        Task<string> GetData(string url);
    }
}
