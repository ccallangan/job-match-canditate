﻿namespace JobMatchCanditate.Models
{
    using Domain.Job;
    using System.Collections.Generic;
    using System.Linq;


    public class JobCandidateViewModel
    {

        public JobCandidateViewModel(Job job, IList<JobCandidate> jobCandidates)
        {
            JobId = job.JobId;
            Company = job.Company;
            Position = job.Name;
            Candidates = jobCandidates.Select((x, index) => CandidateViewModel.ToCandidateViewModel(x, index, job.SkillItems)).ToList();
        }

        public int JobId { get; set; }

        public string Company { get; set; }

        public string Position { get; set; }

        public IList<CandidateViewModel> Candidates { get; set; } = new List<CandidateViewModel>();

    }

    public class CandidateViewModel
    {
        public int Rank { get; set; }

        public string CandidateName { get; set; }

        public IList<CandidateSkill> CandidateSkills { get; set; } = new List<CandidateSkill>();

        public static CandidateViewModel ToCandidateViewModel(JobCandidate jobCandidate, int rank, IList<string> jobSkills)
        {

            return new CandidateViewModel
            {
                Rank = ++rank,
                CandidateName = jobCandidate.CandidateName,
                CandidateSkills = jobCandidate.Skills.Select(x => CandidateSkill.ToCandidateSkill(x, jobSkills)).ToList()
            };
        }
    }

    public class CandidateSkill
    {
        public string Name { get; set; }

        public bool IsJobSkillMatched { get; set; }

        public static CandidateSkill ToCandidateSkill(string canditateSkill, IList<string> jobSkills)
        {
            return new CandidateSkill
            {
                Name = canditateSkill,
                IsJobSkillMatched = jobSkills.Any(x => x.ToLower() == canditateSkill.ToLower())
            };
        }
    }
}