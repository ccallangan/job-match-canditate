﻿namespace JobMatchCanditate.Models
{
    using Domain.Job;

    public class JobViewModel
    {

        public int JobId { get; set; }

        public string Name { get; set; }

        public string Company { get; set; }

        public string Skills { get; set; }

        public static JobViewModel ToJobViewModel(Job job)
        {
            return new JobViewModel
            {
                JobId = job.JobId,
                Name = job.Name,
                Company = job.Company,
                Skills = job.Skills
            };
        }

    }
}