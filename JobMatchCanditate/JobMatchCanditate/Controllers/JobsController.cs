﻿namespace JobMatchCanditate.Controllers
{
    using Domain.Job;
    using Infrastructure.JobAdder;
    using JobMatchCanditate.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    [RouteArea("jobs")]
    public class JobsController : Controller
    {
        private readonly IJobAdderService _jobAdderService;
        private readonly IJobCandidateRetriever _jobCandidateRetriever;

        public JobsController(
            IJobAdderService jobAdderService,
            IJobCandidateRetriever jobCandidateRetriever)
        {
            _jobAdderService = jobAdderService;
            _jobCandidateRetriever = jobCandidateRetriever;
        }

        [Route("")]
        public async Task<ActionResult> Index()
        {

            IList<Job> jobItems = await _jobAdderService.GetJobsAsync();

            IList<JobViewModel> jobs =
                jobItems
                .Select(x => JobViewModel.ToJobViewModel(x))
                .ToList();

            return View("JobList", jobs);
        }

        [Route("{jobId}/candidates")]
        public async Task<ActionResult> GetJobCandidateAsync(int jobId)
        {

            Job job = await _jobAdderService.GetJobAsync(jobId);

            IList<JobCandidate> canditates = await _jobCandidateRetriever.GetJobCandidatesAsync(jobId);

            JobCandidateViewModel model = new JobCandidateViewModel(job, canditates);

            return PartialView("JobCandidate", model);
        }

    }
}