﻿(function () {

  var _endPoint = {
    JobCandidates: '/jobs/[jobId]/candidates'
  };

  var initClickEvent = function () {

    $('.view-candidate').click(function () {

      var button = $(this);
      var jobId = button.attr('data-job-id');

      $.ajax({
        url: _endPoint.JobCandidates.replace('[jobId]', jobId),
        cache: false,
        type: 'GET',
        contentType: 'application/json',
        beforeSend: function () {
          $(button).text('Please Wait...');
        },
        error: function (xhr, status, error) {
          $(button).text('View Candidates');
          alert('Unable to retrieve data. please contact administrator')
        },
        success: function (data, textStatus, XMLHttpRequest) {
          $(button).text('View Candidates');
          $('#canditate-container').empty();
          $('#canditate-container').html(data);
          $('#candidate-modal').modal({ backdrop: 'static', keyboard: false }, 'show');
        }
      });

    });

  };


  initClickEvent();

}());
