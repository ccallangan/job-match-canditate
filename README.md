Job Match Candidate - ASP.net MVC framework

This is an ASP.NET MVC web app that will help a recruiter
automatically match candidates to open jobs.

Requirements

- Visual Studio 2017
- ASP.Net Framework 4.6.1
- ASP.Net MVC framework
- IIS
All other requirements will be pulled down via NuGet when you open/build the solution for the first time

Development Dependencies
Git
Git Flow

Running

Rebuid solution to download nuget packages.

Set JobMatchCandidate as start up project.

Start program and go to http://localhost:50689 to enter the job page. 

The job page will display all the open jobs from Api.

If the data is loaded, You can view the candidates for each open jobs from the API.
